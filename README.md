# VulkanTriangleInC

Trying to make a triangle using Vulkan in the C programming language.

Translating C++ into C while keeping track of everything is not as simple as it may seem.

Remember to install the Vulkan SDK from https://vulkan.lunarg.com/ and use the MSVC compiler.

Using the MSVC compiler without installing the SDK may still work in release mode, however the validation layers used in Debug mode will (likely) be missing.

Has been validated with MSVC v143 (2022) and Vulkan SDK 1.3.211.0.