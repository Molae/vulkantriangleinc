#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "HelloTriangleApplication.h"

int main() {
    HelloTriangle app = new;

    char* error = app.run();
    if(strcmp(error, "\0") != 0) {
        printf("Error: %s", error);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}