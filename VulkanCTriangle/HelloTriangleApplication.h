#define GLFW_INCLUDE_VULKAN

#include "Header.h"

#ifdef NDEBUG
const int enableValidationLayers = 0;
#else
const int enableValidationLayers = 1;
#endif

/* The run method that runs the entire graphical program */
char *run() {
    VulkanData data = {.physicalDevice = VK_NULL_HANDLE, .framebufferResized = 0};

    initWindow(&data);
    data.error_code = initVulkan(&data);
    if (strcmp(data.error_code, "\0") != 0) {
        return data.error_code;
    }
    mainLoop(&data);
    cleanup(&data);

    return data.error_code;
}

void initWindow(VulkanData *data) {
    glfwInit();

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    data->window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan", NULL, NULL);
    glfwSetWindowUserPointer(data->window, data);
    glfwSetFramebufferSizeCallback(data->window, framebufferResizeCallback);
}

void framebufferResizeCallback(GLFWwindow* window, int width, int height) {
    VulkanData *data = (VulkanData*) glfwGetWindowUserPointer(window);
    data->framebufferResized = 1;
}

char *initVulkan(VulkanData *data) {
    char *error_code = createInstance(data);
    if (strcmp(error_code, "\0") != 0) {
        return error_code;
    }

    error_code = setupDebugMessenger(data);
    if (strcmp(error_code, "\0") != 0) {
        return error_code;
    }

    error_code = createSurface(data);
    if (strcmp(error_code, "\0") != 0) {
        return error_code;
    }

    error_code = pickPhysicalDevice(data);
    if (strcmp(error_code, "\0") != 0) {
        return error_code;
    }

    error_code = createLogicalDevice(data);
    if (strcmp(error_code, "\0") != 0) {
        return error_code;
    }

    error_code = createSwapChain(data);
    if (strcmp(error_code, "\0") != 0) {
        return error_code;
    }

    error_code = createImageViews(data);
    if (strcmp(error_code, "\0") != 0) {
        return error_code;
    }

    error_code = createRenderPass(data);
    if (strcmp(error_code, "\0") != 0) {
        return error_code;
    }

    error_code = createGraphicsPipeline(data);
    if (strcmp(error_code, "\0") != 0) {
        return error_code;
    }

    error_code = createFramebuffers(data);
    if (strcmp(error_code, "\0") != 0) {
        return error_code;
    }

    error_code = createCommandPool(data);
    if (strcmp(error_code, "\0") != 0) {
        return error_code;
    }

    error_code = createCommandBuffers(data);
    if (strcmp(error_code, "\0") != 0) {
        return error_code;
    }

    error_code = createSyncObjects(data);
    if (strcmp(error_code, "\0") != 0) {
        return error_code;
    }

    return error_code;
}

char *createInstance(VulkanData *data) {
    if (enableValidationLayers && !checkValidationLayerSupport()) {
        return "Validation layers requested, but not available!";
    }

    VkApplicationInfo appInfo;
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pNext = NULL;
    appInfo.pApplicationName = "Hello Triangle";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "No Engine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo createInfo;
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.flags = 0;
    createInfo.pApplicationInfo = &appInfo;

    uint32_t glfwExtensionCount = 0;
    const char **glfwExtensions;

    /* Get the required extensions for this instance */
    glfwExtensions = getRequiredExtensions(&glfwExtensionCount);

    /* Put the extension information into the createInfo struct */
    createInfo.enabledExtensionCount = glfwExtensionCount;
    createInfo.ppEnabledExtensionNames = glfwExtensions;

    /* Create validation layers if enabled */
    VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;
    if (enableValidationLayers) {
        createInfo.enabledLayerCount = sizeof validationLayers / sizeof *validationLayers;
        createInfo.ppEnabledLayerNames = validationLayers;

        populateDebugMessengerCreateInfo(&debugCreateInfo);
        createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT *) &debugCreateInfo;
    } else {
        createInfo.enabledLayerCount = 0;

        createInfo.pNext = NULL;
    }

    /* Find the extensions available */
    uint32_t extensionCount = 0;
    vkEnumerateInstanceExtensionProperties(NULL, &extensionCount, NULL);
    VkExtensionProperties *extensions = malloc(extensionCount * sizeof *extensions);
    vkEnumerateInstanceExtensionProperties(NULL, &extensionCount, extensions);

    /* Ensure we have the correct extensions */
    for (int i = 0; i < glfwExtensionCount; i++) {
        int found = 0;
        for (int j = 0; j < extensionCount; j++) {
            if (strcmp(glfwExtensions[i], extensions[j].extensionName) == 0) {
                found = 1;
                continue;
            }
        }
        if (found == 0) {
            printf("%s extension was not found.", glfwExtensions[i]);
        }
    }

    /* Free the allocated memory for the extensions to avoid memory leak */
    free(extensions);

    /* Create the instance */
    if (vkCreateInstance(&createInfo, NULL, &data->instance) != VK_SUCCESS) {
        return "VkInstance Creation Failed.\0";
    }
    return "\0";
}

int checkValidationLayerSupport() {
    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, NULL);

    VkLayerProperties *availableLayers = malloc(sizeof *availableLayers * layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers);

    for (int i = 0; i < sizeof validationLayers / sizeof *validationLayers; i++) {
        int layerFound = 0;

        for (int j = 0; j < layerCount; j++) {
            if (strcmp(validationLayers[i], availableLayers[j].layerName) == 0) {
                layerFound = 1;
                break;
            }
        }

        if (!layerFound) {
            free(availableLayers);
            return 0;
        }
    }

    free(availableLayers);
    return 1;
}

const char **getRequiredExtensions(unsigned int *count) {
    uint32_t glfwExtensionCount = 0;
    const char **glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    const char **extensions = malloc(sizeof *extensions * (glfwExtensionCount + 1));
    for (int i = 0; i < glfwExtensionCount; i++) {
        extensions[i] = glfwExtensions[i];
    }

    if (enableValidationLayers) {
        /* Add the extra item */
        extensions[glfwExtensionCount] = VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
        /* Set the count to glfwExtensionCount + 1 because we have added an extra item */
        *count = glfwExtensionCount + 1;
    } else {
        /* Shrink the array if no extra item is needed */
        extensions = realloc(extensions, sizeof *extensions * glfwExtensionCount);
        /* Set the count to glfwExtensionCount because we have not added an extra item */
        *count = glfwExtensionCount;
    }

    return extensions;
}

char *setupDebugMessenger(VulkanData *data) {
    if (!enableValidationLayers) return "\0";

    VkDebugUtilsMessengerCreateInfoEXT createInfo;
    populateDebugMessengerCreateInfo(&createInfo);

    if (CreateDebugUtilsMessengerEXT(data->instance, &createInfo, NULL, &data->debugMessenger) != VK_SUCCESS) {
        return "failed to set up debug messenger!";
    }
    return "\0";
}

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
        void *pUserData) {

    if (messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
        /* Message is important enough to show */
        printf("validation layer: %s\n", pCallbackData->pMessage);
    }

    return VK_FALSE;
}

char *createSurface(VulkanData *data) {
    if (glfwCreateWindowSurface(data->instance, data->window, NULL, &data->surface) != VK_SUCCESS) {
        return "failed to create window surface!";
    }

    return "\0";
}

char *pickPhysicalDevice(VulkanData *data) {
    uint32_t deviceCount = 0;
    vkEnumeratePhysicalDevices(data->instance, &deviceCount, NULL);

    if (deviceCount == 0) {
        return "Failed to find GPUs with Vulkan support!";
    }

    VkPhysicalDevice *devices = malloc(sizeof devices * deviceCount);
    vkEnumeratePhysicalDevices(data->instance, &deviceCount, devices);

    uint32_t maxScore = 0;
    for (int i = 0; i < deviceCount; i++) {
        uint32_t tempScore = isDeviceSuitable(data, devices[i]);
        if (tempScore > maxScore) {
            data->physicalDevice = devices[i];
            maxScore = tempScore;
            break;
        }
    }

    if (data->physicalDevice == VK_NULL_HANDLE || maxScore == 0) {
        return "Failed to find a suitable GPU!";
    }

    return "\0";
}

uint32_t isDeviceSuitable(VulkanData *data, VkPhysicalDevice physicalDevice) {
    /* Query for device information, like name and type */
    VkPhysicalDeviceProperties deviceProperties;
    vkGetPhysicalDeviceProperties(physicalDevice, &deviceProperties);

    /* Query for device features such as 64 bit floating point numbers, multi viewport rendering etc. */
    VkPhysicalDeviceFeatures deviceFeatures;
    vkGetPhysicalDeviceFeatures(physicalDevice, &deviceFeatures);

    /* We like geometry shaders - If the device does not have geometry shaders we do not use it */
    if (!deviceFeatures.geometryShader) {
        return 0;
    }

    /* We *need* the right graphics family, so if not we return 0 */
    QueueFamilyIndices indices = findQueueFamilies(data, physicalDevice);
    if (!indices.queueFamilyIsCompleted(indices.graphicsHasValue, indices.presentHasValue)) {
        return 0;
    }

    /* We want a swap chain because we want to see what we are doing */
    int extensionsSupported = checkDeviceExtensionSupport(physicalDevice);
    if (!extensionsSupported) {
        return 0;
    } else {
        int swapChainAdequate = 0;
        SwapChainSupportDetails swapChainSupport = querySwapChainSupport(data, physicalDevice);
        swapChainAdequate = swapChainSupport.formats != NULL && swapChainSupport.presentModes != NULL;
        if (!swapChainAdequate) {
            return 0;
        }
    }

    /* Get a score for the device */
    uint32_t score = 0;

    /* Dedicated GPUs are great! */
    if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
        score += 1000;
    }

    /* Max size of textures should be as high as possible */
    score += deviceProperties.limits.maxImageDimension2D;

    /* Return the score */
    return score;
}

QueueFamilyIndices findQueueFamilies(VulkanData *data, VkPhysicalDevice physicalDevice) {
    QueueFamilyIndices indices = newQ;

    // Logic to find queue family indices to populate struct with
    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, NULL);

    VkQueueFamilyProperties *queueFamilies = malloc(sizeof *queueFamilies * queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilies);

    for (int i = 0; i < queueFamilyCount; i++) {
        if (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
            indices.graphicsFamily = i;
            indices.graphicsHasValue = 1;
        }

        VkBool32 presentSupport = 0;
        vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, data->surface, &presentSupport);

        if (presentSupport) {
            indices.presentFamily = i;
            indices.presentHasValue = 1;
        }

        if (indices.queueFamilyIsCompleted(indices.graphicsHasValue, indices.presentHasValue)) {
            break;
        }
    }

    free(queueFamilies);

    return indices;
}

int checkDeviceExtensionSupport(VkPhysicalDevice physicalDevice) {
    uint32_t extensionCount;
    vkEnumerateDeviceExtensionProperties(physicalDevice, NULL, &extensionCount, NULL);

    VkExtensionProperties *availableExtensions = malloc(sizeof *availableExtensions * extensionCount);
    vkEnumerateDeviceExtensionProperties(physicalDevice, NULL, &extensionCount, availableExtensions);

    int count = 0;

    for (int i = 0; i < extensionCount; i++) {
        for (int j = 0; j < sizeof deviceExtensions / sizeof *deviceExtensions; j++) {
            if (strcmp(availableExtensions[i].extensionName, deviceExtensions[j]) == 0) {
                count++;
                break;
            }
        }
    }

    free(availableExtensions);

    return count == sizeof deviceExtensions / sizeof *deviceExtensions;
}

char *
createLogicalDevice(VulkanData *data) {
    QueueFamilyIndices indices = findQueueFamilies(data, data->physicalDevice);

    /* The set of unique queue families - And various utility around it */
    int uniqueFamilyCount = 0;
    int isSame = indices.graphicsFamily == indices.presentFamily;
    if (isSame) {
        uniqueFamilyCount = 1;
    } else {
        uniqueFamilyCount = 2;
    }
    uint32_t *uniqueQueueFamilies = malloc(sizeof(uint32_t) * uniqueFamilyCount);
    uniqueQueueFamilies[0] = indices.graphicsFamily;
    if (!isSame) {
        uniqueQueueFamilies[1] = indices.presentFamily;
    }

    /* The queueCreateInfos we want to create */
    VkDeviceQueueCreateInfo *queueCreateInfos = malloc(sizeof *queueCreateInfos * uniqueFamilyCount);

    float queuePriority = 1.0f;
    for (int i = 0; i < uniqueFamilyCount; i++) {
        VkDeviceQueueCreateInfo queueCreateInfo;
        queueCreateInfo.flags = 0;
        queueCreateInfo.pNext = NULL;
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = uniqueQueueFamilies[i];
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &queuePriority;
        queueCreateInfos[i] = queueCreateInfo;
    }

    free(uniqueQueueFamilies);

    /* Query for device features such as 64 bit floating point numbers, multi viewport rendering etc. */
    VkPhysicalDeviceFeatures deviceFeatures;
    vkGetPhysicalDeviceFeatures(data->physicalDevice, &deviceFeatures);

    VkDeviceCreateInfo createInfo;
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

    createInfo.queueCreateInfoCount = uniqueFamilyCount;
    createInfo.pQueueCreateInfos = queueCreateInfos;

    createInfo.pEnabledFeatures = &deviceFeatures;

    createInfo.enabledExtensionCount = sizeof deviceExtensions / sizeof *deviceExtensions;
    createInfo.ppEnabledExtensionNames = deviceExtensions;

    if (enableValidationLayers) {
        createInfo.enabledLayerCount = sizeof validationLayers / sizeof *validationLayers;
        createInfo.ppEnabledLayerNames = validationLayers;
    } else {
        createInfo.enabledLayerCount = 0;
    }

    createInfo.pNext = NULL;
    createInfo.flags = 0;

    if (vkCreateDevice(data->physicalDevice, &createInfo, NULL, &data->device) != VK_SUCCESS) {
        return "failed to create logical device!";
    }

    vkGetDeviceQueue(data->device, indices.graphicsFamily, 0, &data->graphicsQueue);
    vkGetDeviceQueue(data->device, indices.presentFamily, 0, &data->presentQueue);

    return "\0";
}

SwapChainSupportDetails querySwapChainSupport(VulkanData *data, VkPhysicalDevice physicalDevice) {
    SwapChainSupportDetails details;

    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, data->surface, &details.capabilities);

    uint32_t formatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, data->surface, &formatCount, NULL);

    if (formatCount != 0) {
        details.formats = malloc(sizeof *details.formats * formatCount);
        details.formatsCount = formatCount;
        vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, data->surface, &formatCount, details.formats);
    }

    uint32_t presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, data->surface, &presentModeCount, NULL);

    if (presentModeCount != 0) {
        details.presentModes = malloc(sizeof *details.presentModes * presentModeCount);
        details.presentModesCount = presentModeCount;
        vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, data->surface, &presentModeCount,
                                                  details.presentModes);
    }

    return details;
}

VkSurfaceFormatKHR chooseSwapSurfaceFormat(SwapChainSupportDetails *swapChainSupportDetails) {
    for (int i = 0; i < swapChainSupportDetails->formatsCount; i++) {
        if (swapChainSupportDetails->formats[i].format == VK_FORMAT_B8G8R8A8_SRGB &&
            swapChainSupportDetails->formats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
            return swapChainSupportDetails->formats[i];
        }
    }

    return swapChainSupportDetails->formats[0];
}

VkPresentModeKHR chooseSwapPresentMode(SwapChainSupportDetails *swapChainSupportDetails) {
    for (int i = 0; i < swapChainSupportDetails->presentModesCount; i++) {
        if (swapChainSupportDetails->presentModes[i] == PREFERRED_PRESENT_MODE) {
            return swapChainSupportDetails->presentModes[i];
        }
    }

    return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D chooseSwapExtent(VulkanData *data, const VkSurfaceCapabilitiesKHR *capabilities) {
    if (capabilities->currentExtent.width != 0xFFFFFFFF) {
        return capabilities->currentExtent;
    } else {
        int width, height;
        glfwGetFramebufferSize(data->window, &width, &height);

        VkExtent2D actualExtent = {
                width,
                height
        };

        actualExtent.width = clamp(actualExtent.width, capabilities->minImageExtent.width,
                                   capabilities->maxImageExtent.width);
        actualExtent.height = clamp(actualExtent.height, capabilities->minImageExtent.height,
                                    capabilities->maxImageExtent.height);

        return actualExtent;
    }
}

char *createSwapChain(VulkanData *data) {
    SwapChainSupportDetails swapChainSupport = querySwapChainSupport(data, data->physicalDevice);

    VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(&swapChainSupport);
    VkPresentModeKHR presentMode = chooseSwapPresentMode(&swapChainSupport);
    VkExtent2D extent = chooseSwapExtent(data, &swapChainSupport.capabilities);

    uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
    if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount) {
        imageCount = swapChainSupport.capabilities.maxImageCount;
    }

    VkSwapchainCreateInfoKHR createInfo;
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = data->surface;
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = surfaceFormat.format;
    createInfo.imageColorSpace = surfaceFormat.colorSpace;
    createInfo.imageExtent = extent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    createInfo.flags = 0;
    createInfo.pNext = NULL;

    QueueFamilyIndices indices = findQueueFamilies(data, data->physicalDevice);
    uint32_t queueFamilyIndices[] = {indices.graphicsFamily, indices.presentFamily};

    if (indices.graphicsFamily != indices.presentFamily) {
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices = queueFamilyIndices;
    } else {
        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.queueFamilyIndexCount = 0;
        createInfo.pQueueFamilyIndices = NULL;
    }

    createInfo.preTransform = swapChainSupport.capabilities.currentTransform;

    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

    createInfo.presentMode = presentMode;
    createInfo.clipped = VK_TRUE;

    createInfo.oldSwapchain = NULL;

    if (vkCreateSwapchainKHR(data->device, &createInfo, NULL, &data->swapChain) != VK_SUCCESS) {
        return "failed to create swap chain!";
    }

    vkGetSwapchainImagesKHR(data->device, data->swapChain, &imageCount, NULL);
    data->swapChainImages = malloc(sizeof data->swapChainImages * imageCount);
    data->swapChainImagesAmount = imageCount;
    vkGetSwapchainImagesKHR(data->device, data->swapChain, &imageCount, data->swapChainImages);

    data->swapChainImageFormat = surfaceFormat.format;
    data->swapChainExtent = extent;

    return "\0";
}

char *
createImageViews(VulkanData *data) {
    data->swapChainImageViews = malloc(sizeof data->swapChainImageViews * data->swapChainImagesAmount);

    for (size_t i = 0; i < data->swapChainImagesAmount; i++) {
        VkImageViewCreateInfo createInfo;
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createInfo.pNext = NULL;
        createInfo.flags = 0;
        createInfo.image = data->swapChainImages[i];
        createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        createInfo.format = data->swapChainImageFormat;
        createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        createInfo.subresourceRange.baseMipLevel = 0;
        createInfo.subresourceRange.levelCount = 1;
        createInfo.subresourceRange.baseArrayLayer = 0;
        createInfo.subresourceRange.layerCount = 1;

        if (vkCreateImageView(data->device, &createInfo, NULL, &data->swapChainImageViews[i]) != VK_SUCCESS) {
            return "failed to create image views!";
        }
    }

    return "\0";
}

char *createRenderPass(VulkanData *data) {
    VkAttachmentDescription colorAttachment;
    colorAttachment.flags = 0;
    colorAttachment.format = data->swapChainImageFormat;
    colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    VkAttachmentReference colorAttachmentRef;
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass;
    subpass.flags = 0;
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;
    subpass.inputAttachmentCount = 0;
    subpass.pInputAttachments = NULL;
    subpass.pResolveAttachments = NULL;
    subpass.pDepthStencilAttachment = NULL;
    subpass.preserveAttachmentCount = 0;
    subpass.pPreserveAttachments = NULL;

    VkSubpassDependency dependency;
    dependency.dependencyFlags = 0;
    dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependency.dstSubpass = 0;
    dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.srcAccessMask = 0;
    dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    VkRenderPassCreateInfo renderPassInfo;
    renderPassInfo.pNext = NULL;
    renderPassInfo.flags = 0;
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = 1;
    renderPassInfo.pAttachments = &colorAttachment;
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;
    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &dependency;

    if (vkCreateRenderPass(data->device, &renderPassInfo, NULL, &data->renderPass) != VK_SUCCESS) {
        return "failed to create render pass!";
    }

    return "\0";
}

char *createGraphicsPipeline(VulkanData *data) {
    size_t vertShaderSize = 0;
    char *vertShaderCode = readFile("../shaders/vert.spv", &vertShaderSize);
    size_t fragShaderSize = 0;
    char *fragShaderCode = readFile("../shaders/frag.spv", &fragShaderSize);

    VkShaderModule vertShaderModule = createShaderModule(data, vertShaderCode, vertShaderSize);
    VkShaderModule fragShaderModule = createShaderModule(data, fragShaderCode, fragShaderSize);

    VkPipelineShaderStageCreateInfo vertShaderStageInfo;
    vertShaderStageInfo.pNext = NULL;
    vertShaderStageInfo.flags = 0;
    vertShaderStageInfo.pSpecializationInfo = NULL;
    vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
    vertShaderStageInfo.module = vertShaderModule;
    vertShaderStageInfo.pName = "main";

    VkPipelineShaderStageCreateInfo fragShaderStageInfo;
    fragShaderStageInfo.pNext = NULL;
    fragShaderStageInfo.flags = 0;
    fragShaderStageInfo.pSpecializationInfo = NULL;
    fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    fragShaderStageInfo.module = fragShaderModule;
    fragShaderStageInfo.pName = "main";

    VkPipelineShaderStageCreateInfo shaderStages[] = {vertShaderStageInfo, fragShaderStageInfo};

    VkPipelineVertexInputStateCreateInfo vertexInputInfo;
    vertexInputInfo.pNext = NULL;
    vertexInputInfo.flags = 0;
    vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputInfo.vertexBindingDescriptionCount = 0;
    vertexInputInfo.pVertexBindingDescriptions = NULL;
    vertexInputInfo.vertexAttributeDescriptionCount = 0;
    vertexInputInfo.pVertexAttributeDescriptions = NULL;

    VkPipelineInputAssemblyStateCreateInfo inputAssembly;
    inputAssembly.pNext = NULL;
    inputAssembly.flags = 0;
    inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    inputAssembly.primitiveRestartEnable = VK_FALSE;

    VkViewport viewport;
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = (float) data->swapChainExtent.width;
    viewport.height = (float) data->swapChainExtent.height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    VkRect2D scissor;
    scissor.offset.x = 0;
    scissor.offset.y = 0;
    scissor.extent = data->swapChainExtent;

    VkPipelineViewportStateCreateInfo viewportState;
    viewportState.pNext = NULL;
    viewportState.flags = 0;
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    VkPipelineRasterizationStateCreateInfo rasterizer;
    rasterizer.pNext = NULL;
    rasterizer.flags = 0;
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer.lineWidth = 1.0f;
    rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
    rasterizer.depthBiasEnable = VK_FALSE;
    rasterizer.depthBiasConstantFactor = 0.0f;
    rasterizer.depthBiasClamp = 0.0f;
    rasterizer.depthBiasSlopeFactor = 0.0f;

    VkPipelineMultisampleStateCreateInfo multisampling;
    multisampling.pNext = NULL;
    multisampling.flags = 0;
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampling.minSampleShading = 1.0f;
    multisampling.pSampleMask = NULL;
    multisampling.alphaToCoverageEnable = VK_FALSE;
    multisampling.alphaToOneEnable = VK_FALSE;

    VkPipelineColorBlendAttachmentState colorBlendAttachment;
    colorBlendAttachment.colorWriteMask =
            VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachment.blendEnable = VK_FALSE;
    colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
    colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
    colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

    VkPipelineColorBlendStateCreateInfo colorBlending;
    colorBlending.pNext = NULL;
    colorBlending.flags = 0;
    colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.logicOp = VK_LOGIC_OP_COPY;
    colorBlending.attachmentCount = 1;
    colorBlending.pAttachments = &colorBlendAttachment;
    colorBlending.blendConstants[0] = 0.0f;
    colorBlending.blendConstants[1] = 0.0f;
    colorBlending.blendConstants[2] = 0.0f;
    colorBlending.blendConstants[3] = 0.0f;

    VkPipelineLayoutCreateInfo pipelineLayoutInfo;
    pipelineLayoutInfo.pNext = NULL;
    pipelineLayoutInfo.flags = 0;
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = 0;
    pipelineLayoutInfo.pSetLayouts = NULL;
    pipelineLayoutInfo.pushConstantRangeCount = 0;
    pipelineLayoutInfo.pPushConstantRanges = NULL;

    if (vkCreatePipelineLayout(data->device, &pipelineLayoutInfo, NULL, &data->pipelineLayout) != VK_SUCCESS) {
        return "failed to create pipeline layout!";
    }

    VkGraphicsPipelineCreateInfo pipelineInfo;
    pipelineInfo.pNext = NULL;
    pipelineInfo.flags = 0;
    pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount = 2;
    pipelineInfo.pStages = shaderStages;
    pipelineInfo.pVertexInputState = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pDepthStencilState = NULL;
    pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.pDynamicState = NULL;
    pipelineInfo.layout = data->pipelineLayout;
    pipelineInfo.renderPass = data->renderPass;
    pipelineInfo.subpass = 0;
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
    pipelineInfo.basePipelineIndex = -1;

    if (vkCreateGraphicsPipelines(data->device, VK_NULL_HANDLE, 1, &pipelineInfo, NULL, &data->graphicsPipeline) !=
        VK_SUCCESS) {
        return "failed to create graphics pipeline!";
    }

    vkDestroyShaderModule(data->device, fragShaderModule, NULL);
    vkDestroyShaderModule(data->device, vertShaderModule, NULL);
    return "\0";
}


/* Creating a shader module */
VkShaderModule createShaderModule(VulkanData *data, char *code, uint32_t codeSize) {
    VkShaderModuleCreateInfo createInfo;
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = codeSize;
    createInfo.pCode = (const uint32_t *) code;
    createInfo.flags = 0;
    createInfo.pNext = NULL;

    VkShaderModule shaderModule;
    if (vkCreateShaderModule(data->device, &createInfo, NULL, &shaderModule) != VK_SUCCESS) {
        printf("failed to create shader module!");
    }
    free(code);

    return shaderModule;
}

char *createFramebuffers(VulkanData *data) {
    data->swapChainFramebuffers = malloc(sizeof data->swapChainImageViews * data->swapChainImagesAmount);

    for (size_t i = 0; i < data->swapChainImagesAmount; i++) {
        VkImageView attachments[] = {
                data->swapChainImageViews[i]
        };

        VkFramebufferCreateInfo framebufferInfo;
        framebufferInfo.flags = 0;
        framebufferInfo.pNext = NULL;
        framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.renderPass = data->renderPass;
        framebufferInfo.attachmentCount = 1;
        framebufferInfo.pAttachments = attachments;
        framebufferInfo.width = data->swapChainExtent.width;
        framebufferInfo.height = data->swapChainExtent.height;
        framebufferInfo.layers = 1;

        if (vkCreateFramebuffer(data->device, &framebufferInfo, NULL, &data->swapChainFramebuffers[i]) != VK_SUCCESS) {
            return "failed to create framebuffer!";
        }
    }

    return "\0";
}

char *createCommandPool(VulkanData *data) {
    QueueFamilyIndices queueFamilyIndices = findQueueFamilies(data, data->physicalDevice);

    VkCommandPoolCreateInfo poolInfo;
    poolInfo.pNext = NULL;
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    if (queueFamilyIndices.graphicsHasValue) {
        poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;
    }

    if (vkCreateCommandPool(data->device, &poolInfo, NULL, &data->commandPool) != VK_SUCCESS) {
        return "failed to create command pool!";
    }

    return "\0";
}

char *createCommandBuffers(VulkanData *data) {
    data->commandBuffers;
    VkCommandBufferAllocateInfo allocInfo;
    allocInfo.pNext = NULL;
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = data->commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = MAX_FRAMES_IN_FLIGHT;

    if (vkAllocateCommandBuffers(data->device, &allocInfo, data->commandBuffers) != VK_SUCCESS) {
        return "failed to allocate command buffers!";
    }

    return "\0";
}

char *recordCommandBuffer(VulkanData *data, VkCommandBuffer commandBuffer, uint32_t imageIndex) {
    VkCommandBufferBeginInfo beginInfo;
    beginInfo.pNext = NULL;
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = 0;
    beginInfo.pInheritanceInfo = NULL;

    if (vkBeginCommandBuffer(commandBuffer, &beginInfo) != VK_SUCCESS) {
        return "failed to begin recording command buffer!";
    }

    VkRenderPassBeginInfo renderPassInfo;
    renderPassInfo.pNext = NULL;
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass = data->renderPass;
    renderPassInfo.framebuffer = data->swapChainFramebuffers[imageIndex];
    renderPassInfo.renderArea.offset.x = 0;
    renderPassInfo.renderArea.offset.y = 0;
    renderPassInfo.renderArea.extent = data->swapChainExtent;
    VkClearValue clearColor = {{{0.0f, 0.0f, 0.0f, 1.0f}}};
    renderPassInfo.clearValueCount = 1;
    renderPassInfo.pClearValues = &clearColor;

    vkCmdBeginRenderPass(commandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, data->graphicsPipeline);
    vkCmdDraw(commandBuffer, 3, 1, 0, 0);

    vkCmdEndRenderPass(commandBuffer);

    if (vkEndCommandBuffer(commandBuffer) != VK_SUCCESS) {
        return "failed to record command buffer!";
    }

    return "\0";
}

char *createSyncObjects(VulkanData *data) {
    VkSemaphoreCreateInfo semaphoreInfo;
    semaphoreInfo.pNext = NULL;
    semaphoreInfo.flags = 0;
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fenceInfo;
    fenceInfo.pNext = NULL;
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        if (vkCreateSemaphore(data->device, &semaphoreInfo, NULL, &data->imageAvailableSemaphores[i]) != VK_SUCCESS ||
            vkCreateSemaphore(data->device, &semaphoreInfo, NULL, &data->renderFinishedSemaphores[i]) != VK_SUCCESS ||
            vkCreateFence(data->device, &fenceInfo, NULL, &data->inFlightFences[i]) != VK_SUCCESS) {
            return "failed to create semaphores!";
        }
    }

    return "\0";
}

void mainLoop(VulkanData *data) {
    /* initialize the currentFrame variable */
    data->currentFrame = 0;

    while (!glfwWindowShouldClose(data->window)) {
        glfwPollEvents();
        drawFrame(data);
    }

    vkDeviceWaitIdle(data->device);
}

void drawFrame(VulkanData *data) {
    vkWaitForFences(data->device, 1, &data->inFlightFences[data->currentFrame], VK_TRUE, UINT64_MAX);

    uint32_t imageIndex;
    VkResult result = vkAcquireNextImageKHR(data->device, data->swapChain, UINT64_MAX,
                                            data->imageAvailableSemaphores[data->currentFrame], VK_NULL_HANDLE,
                                            &imageIndex);

    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        recreateSwapChain(data);
        return;
    } else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
        printf("failed to acquire swap chain image!");
        return;
    }

    vkResetFences(data->device, 1, &data->inFlightFences[data->currentFrame]);

    vkResetCommandBuffer(data->commandBuffers[data->currentFrame], 0);

    recordCommandBuffer(data, data->commandBuffers[data->currentFrame], imageIndex);

    VkSubmitInfo submitInfo;
    submitInfo.pNext = NULL;
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    VkSemaphore waitSemaphores[] = {data->imageAvailableSemaphores[data->currentFrame]};
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = waitSemaphores;
    submitInfo.pWaitDstStageMask = waitStages;

    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &data->commandBuffers[data->currentFrame];

    VkSemaphore signalSemaphores[] = {data->renderFinishedSemaphores[data->currentFrame]};
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = signalSemaphores;

    if (vkQueueSubmit(data->graphicsQueue, 1, &submitInfo, data->inFlightFences[data->currentFrame]) != VK_SUCCESS) {
        printf("failed to submit draw command buffer!");
    }

    VkPresentInfoKHR presentInfo;
    presentInfo.pNext = NULL;
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = signalSemaphores;
    VkSwapchainKHR swapChains[] = {data->swapChain};
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;
    presentInfo.pImageIndices = &imageIndex;
    presentInfo.pResults = NULL;

    result = vkQueuePresentKHR(data->presentQueue, &presentInfo);

    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || data->framebufferResized) {
        data->framebufferResized = 0;
        recreateSwapChain(data);
    } else if (result != VK_SUCCESS) {
        printf("failed to present swap chain image!");
        return;
    }

    data->currentFrame = (data->currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}

void recreateSwapChain(VulkanData *data) {
    int width = 0, height = 0;
    glfwGetFramebufferSize(data->window, &width, &height);
    while (width == 0 || height == 0) {
        glfwGetFramebufferSize(data->window, &width, &height);
        glfwWaitEvents();
    }

    vkDeviceWaitIdle(data->device);

    cleanupSwapChain(data);

    createSwapChain(data);
    createImageViews(data);
    createRenderPass(data);
    createGraphicsPipeline(data);
    createFramebuffers(data);
}

void cleanupSwapChain(VulkanData *data) {
    for (int i = 0; i < data->swapChainImagesAmount; i++) {
        vkDestroyFramebuffer(data->device, data->swapChainFramebuffers[i], NULL);
    }

    vkDestroyPipeline(data->device, data->graphicsPipeline, NULL);
    vkDestroyPipelineLayout(data->device, data->pipelineLayout, NULL);
    vkDestroyRenderPass(data->device, data->renderPass, NULL);

    for (int i = 0; i < data->swapChainImagesAmount; i++) {
        vkDestroyImageView(data->device, data->swapChainImageViews[i], NULL);
    }

    vkDestroySwapchainKHR(data->device, data->swapChain, NULL);
}

void cleanup(VulkanData *data) {
    cleanupSwapChain(data);

    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        vkDestroySemaphore(data->device, data->imageAvailableSemaphores[i], NULL);
        vkDestroySemaphore(data->device, data->renderFinishedSemaphores[i], NULL);
        vkDestroyFence(data->device, data->inFlightFences[i], NULL);
    }

    vkDestroyCommandPool(data->device, data->commandPool, NULL);

    vkDestroyDevice(data->device, NULL);

    if (enableValidationLayers) {
        DestroyDebugUtilsMessengerEXT(data->instance, data->debugMessenger, NULL);
    }

    vkDestroySurfaceKHR(data->instance, data->surface, NULL);
    vkDestroyInstance(data->instance, NULL);

    glfwDestroyWindow(data->window);

    glfwTerminate();
}