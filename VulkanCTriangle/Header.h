#ifndef VULKANCTRIANGLE_HEADER_H
#define VULKANCTRIANGLE_HEADER_H

#include <GLFW/glfw3.h>

/* Constants */
const int WIDTH = 800;
const int HEIGHT = 600;
#define MAX_FRAMES_IN_FLIGHT 2

/*
 * Options:
 * VK_PRESENT_MODE_IMMEDIATE_KHR - Immediate, no v-sync
 * VK_PRESENT_MODE_FIFO_KHR - Ordinary v-sync, guaranteed to exist
 * VK_PRESENT_MODE_FIFO_RELAXED_KHR - Relaxed v-sync, image may show up late if not available at refresh
 * VK_PRESENT_MODE_MAILBOX_KHR - Accelerated v-sync, shows image when it is ready
 */
const VkPresentModeKHR PREFERRED_PRESENT_MODE = VK_PRESENT_MODE_IMMEDIATE_KHR;

const char *validationLayers[] = {"VK_LAYER_KHRONOS_validation"};
const char *deviceExtensions[] = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};

/* The struct that holds all the relevant data */
typedef struct VulkanData {
    char *error_code;
    GLFWwindow *window;
    VkInstance instance;
    VkDebugUtilsMessengerEXT debugMessenger;
    VkSurfaceKHR surface;
    VkPhysicalDevice physicalDevice;
    VkDevice device;
    VkQueue graphicsQueue;
    VkQueue presentQueue;
    VkSwapchainKHR swapChain;
    VkImage *swapChainImages;
    uint32_t swapChainImagesAmount;
    VkFormat swapChainImageFormat;
    VkExtent2D swapChainExtent;
    VkImageView *swapChainImageViews;
    VkRenderPass renderPass;
    VkPipelineLayout pipelineLayout;
    VkPipeline graphicsPipeline;
    VkFramebuffer *swapChainFramebuffers;
    VkCommandPool commandPool;
    VkCommandBuffer commandBuffers[MAX_FRAMES_IN_FLIGHT];
    VkSemaphore imageAvailableSemaphores[MAX_FRAMES_IN_FLIGHT];
    VkSemaphore renderFinishedSemaphores[MAX_FRAMES_IN_FLIGHT];
    VkFence inFlightFences[MAX_FRAMES_IN_FLIGHT];
    int framebufferResized;
    uint32_t currentFrame;
} VulkanData;

/* For QueueFamily */
int queueFamilyIsCompleted(int a, int b) {
    return a && b;
}

struct QueueFamilyIndices {
    uint32_t graphicsFamily;
    uint32_t presentFamily;
    int graphicsHasValue;
    int presentHasValue;

    int (*queueFamilyIsCompleted)(int, int);
} newQ = {0, 0, 0, 0, queueFamilyIsCompleted};

typedef struct QueueFamilyIndices QueueFamilyIndices;

/* For swap chain */
typedef struct SwapChainSupportDetails {
    VkSurfaceCapabilitiesKHR capabilities;
    VkSurfaceFormatKHR *formats;
    uint32_t formatsCount;
    VkPresentModeKHR *presentModes;
    uint32_t presentModesCount;
} SwapChainSupportDetails;

/* Prototypes */
char *run();

void initWindow(VulkanData *data);

void framebufferResizeCallback(GLFWwindow* window, int width, int height);

char *initVulkan(VulkanData *data);

char *createInstance(VulkanData *data);

int checkValidationLayerSupport();

const char **getRequiredExtensions(unsigned int *count);

char *setupDebugMessenger(VulkanData *data);

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                                                    VkDebugUtilsMessageTypeFlagsEXT messageType,
                                                    const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
                                                    void *pUserData);

char *createSurface(VulkanData *data);

char *pickPhysicalDevice(VulkanData *data);

uint32_t isDeviceSuitable(VulkanData *data, VkPhysicalDevice device);

QueueFamilyIndices findQueueFamilies(VulkanData *data, VkPhysicalDevice physicalDevice);

int checkDeviceExtensionSupport(VkPhysicalDevice physicalDevice);

char *
createLogicalDevice(VulkanData *data);

SwapChainSupportDetails querySwapChainSupport(VulkanData *data, VkPhysicalDevice physicalDevice);

char *createSwapChain(VulkanData *data);

char *createImageViews(VulkanData *data);

char *createRenderPass(VulkanData *data);

char *createGraphicsPipeline(VulkanData *data);

VkShaderModule createShaderModule(VulkanData *data, char *code, uint32_t codeSize);

char *createFramebuffers(VulkanData *data);

char *createCommandPool(VulkanData *data);

char *createCommandBuffers(VulkanData *data);

char *recordCommandBuffer(VulkanData *data, VkCommandBuffer commandBuffer, uint32_t imageIndex);

char *createSyncObjects(VulkanData *data);

void mainLoop(VulkanData *data);

void drawFrame(VulkanData *data);

void recreateSwapChain(VulkanData *data);

void cleanupSwapChain(VulkanData *data);

void cleanup(VulkanData *data);

/* Initializing the HelloTriangle struct, so we can call the run method through that (to be clean) */
struct HelloTriangle {
    char *(*run)(void);
} new = {run};

typedef struct HelloTriangle HelloTriangle;

/* Global functions */
/* Beautiful small clamp function */
uint32_t clamp(uint32_t d, uint32_t min, uint32_t max) {
    const uint32_t t = d < min ? min : d;
    return t > max ? max : t;
}

/* The proxy function that makes it possible to have the debug callback */
VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT *pCreateInfo,
                                      const VkAllocationCallbacks *pAllocator,
                                      VkDebugUtilsMessengerEXT *pDebugMessenger) {
    PFN_vkCreateDebugUtilsMessengerEXT func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance,
                                                                                                         "vkCreateDebugUtilsMessengerEXT");
    if (func != NULL) {
        return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
    } else {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

/* The proxy function that is used to destroy the debug callback */
void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger,
                                   const VkAllocationCallbacks *pAllocator) {
    PFN_vkDestroyDebugUtilsMessengerEXT func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance,
                                                                                                           "vkDestroyDebugUtilsMessengerEXT");
    if (func != NULL) {
        func(instance, debugMessenger, pAllocator);
    }
}

/* Populates a CreateInfo for the DebugMessenger */
void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT *createInfo) {
    createInfo->sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createInfo->flags = 0;
    createInfo->pNext = NULL;
    createInfo->pUserData = NULL;
    createInfo->messageSeverity =
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    createInfo->messageType =
            VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    createInfo->pfnUserCallback = debugCallback;
}

/* Helper function for loading shader files */
char *readFile(const char *filename, size_t *outSize) {
    FILE *file;
    int open_code = fopen_s(&file, filename, "rb+");

    if (open_code) {
        return "failed to open file!";
    }

    /* Go to the end of the file so we can figure out how big it is */
    fseek(file, 0L, SEEK_END);

    /* Get the size of the file so we can create a buffer */
    size_t fileSize = ftell(file);
    *outSize = fileSize;
    /* Don't know if we need to multiply by sizeof(char*) */
    char *buffer = malloc(sizeof(char *) * fileSize);

    /* Go back to the start of the file and read the file into buffer */
    fseek(file, 0L, SEEK_SET);
    fread(buffer, fileSize, 1, file);

    fclose(file);

    return buffer;
}

#endif //VULKANCTRIANGLE_HEADER_H
